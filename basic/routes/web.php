<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\QueryController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\ContactController;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/about' , function () {return view('about');});

Route::get('/about',[AboutController::class,'index'])->middleware('check')->name('about');
Route::get('/contact',[ContactController::class,'index'])->name('con');
Route::get('/test',[TestController::class,'test'])->name('tes');
Route::get('/home' , function (){
    echo "this is home page for middleware ";
}  );

////Category controller
//Route::get('/category/all',[\App\Http\Controllers\CategoryController::class,'allCategory'])->name('all-category');
// Category Controller
Route::get('/category/all', [CategoryController::class, 'AllCat'])->name('all.category');
Route::post('/category/add', [CategoryController::class, 'AddCat'])->name('store.category');

Route::get('/category/edit/{id}', [CategoryController::class, 'Edit']);
Route::post('/category/update/{id}', [CategoryController::class, 'Update']);
Route::get('/softdelete/category/{id}', [CategoryController::class, 'SoftDelete']);

Route::get('/category/restore/{id}', [CategoryController::class, 'Restore']);
Route::get('/pdelete/category/{id}', [CategoryController::class, 'Pdelete']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    $users = User::all();
    return view('dashboard' , compact('users'));
})->name('dashboard');
Route::post('/category/add', [CategoryController::class, 'AddCat'])->name('store.category');

// student controller
Route::get('/student/all', [StudentController::class, 'AllStu'])->name('all.student');
Route::get('/student/admission/form', [StudentController::class, 'showStudentForm'])->name('admission_form');
Route::post('/student/add', [StudentController::class, 'AddStu'])->name('store.student');
Route::get('/student/edit/{id}', [StudentController::class, 'Edit']);
Route::post('/student/update/{id}', [StudentController::class, 'Update']);
Route::get('/softdelete/student/{id}', [StudentController::class, 'SoftDelete']);
Route::get('/student/restore/{id}', [StudentController::class, 'Restore']);
Route::get('/pdelete/student/{id}', [StudentController::class, 'Pdelete']);
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    $users = User::all();
    return view('dashboard' , compact('users'));
})->name('dashboard');
Route::post('/student/add', [StudentController::class, 'AddStu'])->name('store.student');
Route::post('/student/import', [StudentController::class,'importStudents'])->name('import.students');

//query routes
Route::get('/query/all',[\App\Http\Controllers\QueryController::class,'index'])->name('queries');
Route::post('/query/add',[\App\Http\Controllers\QueryController::class,'store'])->name('store.query');
Route::get('/query/edit/{id}', [QueryController::class, 'Edit']);
Route::post('/query/update/{id}', [QueryController::class, 'Update']);
Route::get('/softdelete/query/{id}', [QueryController::class, 'SoftDelete']);
Route::get('/query/restore/{id}', [QueryController::class, 'Restore']);
Route::get('/pdelete/query/{id}', [QueryController::class, 'Pdelete']);
Route::get('/query/user',[QueryController::class,'showUserQueries']);
