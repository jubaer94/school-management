<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('class')->nullable();
            $table->string('from_no')->nullable();
            $table->string('student_name')->nullable();
            $table->string('student_id')->nullable();

            $table->string('section')->nullable();
            $table->string('sms_contact')->nullable();
            $table->string('father_contact')->nullable();
            $table->string('gender')->nullable();
            $table->string('DOB')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('siblings_id')->nullable();
            $table->string('siblings')->nullable();
            $table->string('email')->nullable();
            $table->string('home_contact')->nullable();
            $table->string('guardian_contact')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('present_address')->nullable();
            $table->string('permanent_address')->nullable();

            $table->timestamps();
            $table->string('nid')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
