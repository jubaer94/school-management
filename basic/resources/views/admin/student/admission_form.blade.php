<x-app-layout>



                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header"> Add Student </div>
                        <div class="card-body">



                            <form action="{{ route('store.student') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Student Name</label>
                                    <input type="text" name="student_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('student_name')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">Student ID</label>
                                    <input type="number" name="student_id" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('student_id')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror


                                    <label for="exampleInputEmail1">class</label>
                                    <input type="text" name="class" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('class')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">from_no</label>
                                    <input type="number" name="from_no" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('from_no')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror


                                    <label for="exampleInputEmail1">section</label>
                                    <input type="text" name="section" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('section')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror


                                    <label for="exampleInputEmail1">sms_contact</label>
                                    <input type="number" name="sms_contact" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('sms_contact')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">father_contact</label>
                                    <input type="number" name="father_contact" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                    @error('father_contact')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                    <label for="exampleInputEmail1">home_contact</label>
                                    <input type="number" name="home_contact" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                    @error('home_contact')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                    <label for="exampleInputEmail1">guardian_contact</label>
                                    <input type="number" name="guardian_contact" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                    @error('guardian_contact')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">gender</label>
                                    <input type="text" name="gender" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('gender')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">DOB</label>
                                    <input type="date" name="DOB" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('DOB')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">mother_name</label>
                                    <input type="text" name="mother_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('mother_name')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">father_name</label>
                                    <input type="text" name="father_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('father_name')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                    <label for="exampleInputEmail1">mother_name</label>
                                    <input type="text" name="mother_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('mother_name')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">present_address</label>
                                    <input type="text" name="present_address" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('present_address')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">permanent_address</label>
                                    <input type="text" name="permanent_address" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('permanent_address')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">siblings_id</label>
                                    <input type="number" name="siblings_id" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('siblings_id')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">siblings</label>
                                    <input type="text" name="siblings" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('siblings')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">email</label>
                                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('email')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">home_contact</label>
                                    <input type="number" name="home_contact" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('home_contact')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">blood_group</label>
                                    <input type="text" name="blood_group" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('blood_group')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                    <label for="exampleInputEmail1">nid</label>
                                    <input type="number" name="nid" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                                    @error('nid')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror

                                </div>

                                <button type="submit" class="btn btn-primary">Add Student</button>
                            </form>

                        </div>

                    </div>
                </div>



            </div>
        </div>







    </div>
</x-app-layout>
