<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">

            All Student<b>  </b>
        </h2>
    </x-slot>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header"> Add Student </div>
            <div class="card-body">



                <form action="{{ route('import.students') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Student Excell file</label>
                        <input type="file" name="file" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                        @error('student_name')
                        <span class="text-danger"> {{ $message }}</span>
                        @enderror

                    <button type="submit" class="btn btn-primary">upload csv</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

            </div>
            </div>
            </div>

        </div>
    </div>



    </div>
    </div>



    <div class="col-md-8">
                    <div class="card">


                        @if(session('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ session('success') }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif


                        <div class="card-header"> All Students </div>
                            <a href="{{ url('student/admission/form') }}" class="btn btn-info">Add Student</a>


                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">SL No</th>
                                <th scope="col">student Name</th>
                                {{--      <th scope="col">User</th>--}}

                                <th scope="col">class</th>
                                <th scope="col">from_no</th>
                                <th scope="col">section</th>
                                <th scope="col">student_id</th>
                                <th scope="col">sms_contact</th>
                                <th scope="col">father_contact</th>
                                <th scope="col">gender</th>
                                <th scope="col">DOB</th>
                                <th scope="col">mother_name</th>
                                <th scope="col">father_name</th>
                                <th scope="col">present_address</th>
                                <th scope="col">permanent_address</th>
                                <th scope="col">siblings_id</th>
                                <th scope="col">siblings</th>
                                <th scope="col">email</th>
                                <th>home_contact</th>
                                <th>blood_group</th>
                                <th>nid</th>
                                <th>created_at</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!-- @php($i = 1) -->
                            @foreach($students as $student)
                                <tr>
                                    <th scope="row"> {{ $students->firstItem()+$loop->index  }} </th>
                                    <td> {{ $student->student_name }} </td>
                                    <td> {{ $student->class }} </td>
                                    <td> {{ $student->from_no }} </td>
                                    <td> {{ $student->section }} </td>
                                    <td> {{ $student->student_id }} </td>
                                    <td> {{ $student->sms_contact }} </td>
                                    <td> {{ $student->father_contact }} </td>
                                    <td> {{ $student->gender }} </td>
                                    <td> {{ $student->DOB }} </td>
                                    <td> {{ $student->mother_name }} </td>
                                    <td> {{ $student->father_name }} </td>
                                    <td> {{ $student->present_address }} </td>
                                    <td> {{ $student->permanent_address }} </td>
                                    <td> {{ $student->siblings_id }} </td>
                                    <td> {{ $student->siblings }} </td>
                                    <td> {{ $student->email }} </td>
                                    <td> {{ $student->home_contact }} </td>
                                    <td> {{ $student->blood_group }} </td>
                                    <td> {{ $student->nid }} </td>

                                    {{--      <td> {{ $student->user->name }} </td>--}}
                                    <td>
                                        @if($student->created_at ==  NULL)
                                            <span class="text-danger"> No Date Set</span>
                                        @else
                                            {{ Carbon\Carbon::parse($student->created_at)->diffForHumans() }}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('student/edit/'.$student->id) }}" class="btn btn-info">Edit</a>
                                        <a href="{{ url('softdelete/student/'.$student->id) }}" class="btn btn-danger">Delete</a>
                                    </td>


                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                        {{ $students->links() }}

                    </div>
                </div>


{{--                <div class="col-md-4">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-header"> Add Student </div>--}}
{{--                        <div class="card-body">--}}



{{--                            <form action="{{ route('store.student') }}" method="POST">--}}
{{--                                @csrf--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="exampleInputEmail1">Student Name</label>--}}
{{--                                    <input type="text" name="student_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('student_name')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">Student ID</label>--}}
{{--                                    <input type="number" name="student_id" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('student_id')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}


{{--                                    <label for="exampleInputEmail1">class</label>--}}
{{--                                    <input type="text" name="class" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('class')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">from_no</label>--}}
{{--                                    <input type="number" name="from_no" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('from_no')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}


{{--                                    <label for="exampleInputEmail1">section</label>--}}
{{--                                    <input type="text" name="section" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('section')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}


{{--                                    <label for="exampleInputEmail1">sms_contact</label>--}}
{{--                                    <input type="number" name="sms_contact" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('sms_contact')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">father_contact</label>--}}
{{--                                    <input type="number" name="father_contact" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}
{{--                                    @error('father_contact')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}
{{--                                    <label for="exampleInputEmail1">home_contact</label>--}}
{{--                                    <input type="number" name="home_contact" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}
{{--                                    @error('home_contact')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}
{{--                                    <label for="exampleInputEmail1">guardian_contact</label>--}}
{{--                                    <input type="number" name="guardian_contact" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}
{{--                                    @error('guardian_contact')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">gender</label>--}}
{{--                                    <input type="text" name="gender" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('gender')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">DOB</label>--}}
{{--                                    <input type="date" name="DOB" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('DOB')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">mother_name</label>--}}
{{--                                    <input type="text" name="mother_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('mother_name')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">father_name</label>--}}
{{--                                    <input type="text" name="father_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('father_name')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}
{{--                                    <label for="exampleInputEmail1">mother_name</label>--}}
{{--                                    <input type="text" name="mother_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('mother_name')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">present_address</label>--}}
{{--                                    <input type="text" name="present_address" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('present_address')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">permanent_address</label>--}}
{{--                                    <input type="text" name="permanent_address" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('permanent_address')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">siblings_id</label>--}}
{{--                                    <input type="number" name="siblings_id" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('siblings_id')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">siblings</label>--}}
{{--                                    <input type="text" name="siblings" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('siblings')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">email</label>--}}
{{--                                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('email')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">home_contact</label>--}}
{{--                                    <input type="number" name="home_contact" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('home_contact')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">blood_group</label>--}}
{{--                                    <input type="text" name="blood_group" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('blood_group')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                    <label for="exampleInputEmail1">nid</label>--}}
{{--                                    <input type="number" name="nid" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">--}}

{{--                                    @error('nid')--}}
{{--                                    <span class="text-danger"> {{ $message }}</span>--}}
{{--                                    @enderror--}}

{{--                                </div>--}}

{{--                                <button type="submit" class="btn btn-primary">Add Student</button>--}}
{{--                            </form>--}}

{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}



            </div>
        </div>



        <!-- Trash Part -->

        <div class="container">
            <div class="row">


                <div class="col-md-8">
                    <div class="card">




                        <div class="card-header">Trash List </div>


                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">SL No</th>
                                <th scope="col">Student Name</th>
                                {{--      <th scope="col">User</th>--}}
                                <th scope="col">Created At</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!-- @php($i = 1) -->
                            @foreach($trashStu as $student)
                                <tr>
                                    <th scope="row"> {{ $students->firstItem()+$loop->index  }} </th>
                                    <td> {{ $student->student_name }} </td>
                                    {{--      <td> {{ $student->user->name }} </td>--}}
                                    <td>
                                        @if($student->created_at ==  NULL)
                                            <span class="text-danger"> No Date Set</span>
                                        @else
                                            {{ Carbon\Carbon::parse($student->created_at)->diffForHumans() }}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('student/restore/'.$student->id) }}" class="btn btn-info">Restore</a>
                                        <a href="{{ url('pdelete/student/'.$student->id) }}" class="btn btn-danger">P Delete</a>
                                    </td>


                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                        {{ $trashStu->links() }}

                    </div>
                </div>


                <div class="col-md-4">

                </div>



            </div>
        </div>

        <!-- End Trush -->



    </div>
</x-app-layout>
