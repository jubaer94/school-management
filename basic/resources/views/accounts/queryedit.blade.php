<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">

            Edit Query<b>  </b>


        </h2>
    </x-slot>

    <div class="py-12">
        <div class="container">
            <div class="row">




                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"> Edit Query </div>
                        <div class="card-body">




                            <form action="{{ url('query/update/'.$query->id)}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="item">item</label>
                                    <textarea id="item" name="item" rows="4" cols="50"  >
                                        {{$query->item}}
                                    </textarea>

</textarea>
                                    @error('item')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                </div>
                            <div>
                                    <label for="campus">Choose a campus:</label>

                                    <select name="campus" id="campus">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="kids">kids</option>
                                    </select>

                                </div>

                                <button type="submit" class="btn btn-primary">Add Query</button>
                            </form>

                        </div>

                    </div>
                </div>



            </div>
        </div>

    </div>
</x-app-layout>
