<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            All Category<b>  </b>
        </h2>

    </x-slot>
    <div class="py-12">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        @if(session('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ session('success') }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card-header"> All Qurey </div>
                            <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">SL No</th>
                                <th scope="col">Query No</th>
                                <th scope="col">campus</th>
                                <th scope="col">item</th>
                                {{--      <th scope="col">User</th>--}}
                                <th scope="col">Created At</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!-- @php($i = 1) -->
                            @foreach($queries as $query)
                                <tr>
                                    <th scope="row"> {{ $queries->firstItem()+$loop->index  }} </th>
                                    <td> {{ $query->campus }} </td>
                                    <td> {{ $query->query_no }} </td>
                                    <td> {{ $query->item }} </td>
                                    {{--      <td> {{ $query->user->name }} </td>--}}
                                    <td>
                                        @if($query->created_at ==  NULL)
                                            <span class="text-danger"> No Date Set</span>
                                        @else
                                            {{ Carbon\Carbon::parse($query->created_at)->diffForHumans() }}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('query/edit/'.$query->id) }}" class="btn btn-info">Edit</a>
                                        <a href="{{ url('softdelete/query/'.$query->id) }}" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $queries->links() }}

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header"> Add Query </div>
                        <div class="card-body">
                            <form action="{{ route('store.query') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="item">item</label>
                                    <textarea id="item" name="item" rows="4" cols="50" >
                                    </textarea>
                                    @error('item')
                                    <span class="text-danger"> {{ $message }}</span>
                                    @enderror
                                    <label for="campus">Choose a car:</label>
                                    <select name="campus" id="campus">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="kids">kids</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">Add Query</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Trash Part -->
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Trash List </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">SL No</th>
                                <th scope="col">Query No</th>
                                <th scope="col">campus</th>
                                <th scope="col">item</th>
                                {{--      <th scope="col">User</th>--}}
                                <th scope="col">Created At</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!-- @php($i = 1) -->
                            @foreach($trashQueries as $query)
                                <tr>
                                    <td> {{ $query->campus }} </td>
                                    <td> {{ $query->query_no }} </td>
                                    <td> {{ $query->item }} </td>
                                    <td>
                                        @if($query->created_at ==  NULL)
                                            <span class="text-danger"> No Date Set</span>
                                        @else
                                            {{ Carbon\Carbon::parse($query->created_at)->diffForHumans() }}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('query/restore/'.$query->id) }}" class="btn btn-info">Restore</a>
                                        <a href="{{ url('pdelete/query/'.$query->id) }}" class="btn btn-danger">P Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $trashQueries->links() }}

                    </div>
                </div>
                <div class="col-md-4">
                </div>
            </div>
        </div>

        <!-- End Trush -->

    </div>
</x-app-layout>
