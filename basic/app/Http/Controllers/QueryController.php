<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Query;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $queries = Query::latest()->paginate(5);
        $trashQueries = Query::onlyTrashed()->latest()->paginate(3);
        return view('accounts.query',compact('queries','trashQueries'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
//        dd($request);
        Query::insert([
            'campus' => $request->campus,
            'user_id' => \auth()->id(),
            'item' => $request->item,
            'query_no'=>rand(0,1000),
            'created_at' => Carbon::now()
        ]);
        return Redirect()->back()->with('success','Query Inserted Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Query  $query
     * @return \Illuminate\Http\Response
     */
    public function show(Query $query)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Query  $query
     * @return Application|Factory|View
     */
    public function Edit($id){
        // $categories = Category::find($id);
        $query = DB::table('queries')->where('id',$id)->first();
        return view('accounts.queryedit',compact('query'));

    }


    public function Update(Request $request ,$id){
        // $update = Category::find($id)->update([
        //     'query_name' => $request->query_name,
        //     'user_id' => Auth::user()->id

        // ]);

        $data = array();
        $data['campus'] = $request->campus;
        $data['item'] = $request->item;

        DB::table('queries')->where('id',$id)->update($data);

        return Redirect()->route('queries')->with('success','query Updated Successfully');

    }

    public function showUserQueries(){
        $id = auth()->id();
        $user = User::find($id);
        $queries = User::find($id)->queries;

        return view('accounts.userqueries',compact('queries'));
}


    public function SoftDelete($id){
        $delete = Query::find($id)->delete();
        return Redirect()->back()->with('success','Query Soft Delete Successfully');
    }


    public function Restore($id){
        $delete = Query::withTrashed()->find($id)->restore();
        return Redirect()->back()->with('success','Query Restore Successfully');

    }

    public function Pdelete($id){
        $delete = Query::onlyTrashed()->find($id)->forceDelete();
        return Redirect()->back()->with('success','Query Permanently Deleted');
    }
}
