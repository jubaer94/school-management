<?php

namespace App\Http\Controllers;

use App\Imports\StudentsImport;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
class StudentController extends Controller
{


    public function __construct(){
        $this->middleware('auth');
    }
    public function allStudent()
    {
        return view('admin.student.index');
    }
    public function AllStu(){
        // $students = DB::table('students')
        //         ->join('users','students.user_id','users.id')
        //         ->select('students.*','users.name')
        //         ->latest()->paginate(5);


        $students = Student::latest()->paginate(5);
        $trashStu = Student::onlyTrashed()->latest()->paginate(3);

        // $students = DB::table('students')->latest()->paginate(5);
        return view('admin.student.index', compact('students','trashStu'));
    }

public function  showStudentForm(){
        return view('admin.student.admission_form');

}

    public function AddStu(Request $request){

//        dd($request);
////        protected $fillable =['class','from_no','student_id','student_name','section','sms_contact','father_contact','gender','DOB','mother_name','father_name','present_address','permanent_address','siblings_id','siblings','email','home_contact','guardian_contact','blood_group','nid'];
//        $validatedData = $request->validate([
//            'student_name' => 'required|unique:students|max:255',
//            'student_id' =>'required|unique:students|min:11|max:255|numeric',
//
//        ],
//            [
//                'student_name.required' => 'Please Input Student Name',
//                'student_name.max' => 'Student name should be less than 255Chars',
//                'student_id.required' => 'Please Input Student id',
//                'student_id.unique' => 'student id should be unique'
//            ]);

        Student::insert([
            'student_name' => $request->student_name,
            'class' => $request->class,
            'from_no' => $request->from_no,
            'student_id' => $request->student_id,
            'section' => $request->section,
            'sms_contact' => $request->sms_contact,
            'father_contact' => $request->father_contact,
            'home_contact'=>$request->home_contact,
            'guardian_contact'=>$request->guardian_contact,
            'gender' => $request->gender,
            'DOB' => $request->DOB,
            'mother_name' => $request->mother_name,
            'father_name' => $request->father_name,
            'present_address' => $request->present_address,
            'permanent_address' => $request->permanent_address,
            'siblings_id' => $request->siblings_id,
            'blood_group'=>$request->blood_group,
            'email' => $request->email,
            'siblings' => $request->siblings,
            'nid'=>$request->nid,
//            'student_name' => $request->student_name,
            'created_at' => Carbon::now()
        ]);

        // $student = new Student;
        // $student->student_name = $request->student_name;
        // $student->user_id = Auth::user()->id;
        // $student->save();

        // $data = array();
        // $data['student_name'] = $request->student_name;
        // $data['user_id'] = Auth::user()->id;
        // DB::table('students')->insert($data);

        return Redirect()->route('all.student')->with('success','Student Inserted Successfully');

    }

    public function importStudents(Request $request)
    {

       Excel::import(new StudentsImport(),$request->file('file'));

//        return Redirect()->route('all.student')->with('success','Student Inserted Successfully');



    }

    public function Edit($id){
        // $students = Student::find($id);
        $student = DB::table('students')->where('id',$id)->first();
        return view('admin.student.edit',compact('student'));

    }


    public function Update(Request $request ,$id){
        // $update = Student::find($id)->update([
        //     'student_name' => $request->student_name,
        //     'user_id' => Auth::user()->id

        // ]);

        $data = array();
        $data['student_name'] = $request->student_name;
        $data['from_no'] = $request->from_no;
        $data['student_id'] = $request->student_id;
        $data['section'] = $request->section;
        $data['sms_contact'] = $request->sms_contact;
        $data['father_contact'] = $request->father_contact;
        $data['home_contact'] = $request->home_contact;
        $data['guardian_contact'] = $request->guardian_contact;
        $data['gender'] = $request->gender;
        $data['DOB'] = $request->DOB;
        $data['mother_name'] = $request->mother_name;
        $data['father_name'] = $request->father_name;
        $data['present_address'] = $request->present_address;
        $data['permanent_address'] = $request->permanent_address;
        $data['siblings_id'] = $request->siblings_id;
        $data['blood_group'] = $request->blood_group;
        $data['email'] = $request->email;
        $data['siblings'] = $request->siblings;
        $data['nid'] = $request->nid;
        $data['created_at'] = $request->created_at;

        $data['class'] = $request->class;


//        $data['user_id'] = Auth::user()->id;
        DB::table('students')->where('id',$id)->update($data);

        return Redirect()->route('all.student')->with('success','Student Updated Successfull');

    }


    public function SoftDelete($id){
        $delete = Student::find($id)->delete();
        return Redirect()->back()->with('success','Student Soft Delete Successfully');
    }


    public function Restore($id){
        $delete = Student::withTrashed()->find($id)->restore();
        return Redirect()->back()->with('success','Student Restore Successfully');

    }

    public function Pdelete($id){
        $delete = Student::onlyTrashed()->find($id)->forceDelete();
        return Redirect()->back()->with('success','Student Permanently Deleted');
    }
}
