<?php

namespace App\Imports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row)
    {

//dd($row);
//        0 => "Sl. "
//  1 => "CLASS"
//  2 => "FORM NO"
//  3 => "ID"
//  4 => "Student Name"
//  5 => "SECTION"
//  6 => "SMS Contact"
//  7 => "Father's Contact"
//  8 => "Mother's Contact"
//  9 => "Gender"
//  10 => "DOB"
//  11 => "Mother's Name"
//  12 => "Father's Name"
//  13 => "Present Address"
//  14 => "Permanent Address"
//  15 => "Siblings ID"
//  16 => "Siblings"
//  17 => "Class"
//  18 => "EMAIL"
//  19 => "Home Contact"
//  20 => "Guardian's Contact"
//  21 => "Relation"
//  22 => "Blood Group"
//  23 => "Father's Occupation"
//  24 => "Age"
//  25 => "Religion"
//  26 => "Nationality"
//  27 => "Disability"
        return new Student([
            'class'     => $row[1],
            'from_no'     => $row[2],
            'student_id'     => $row[3],
            'student_name'     => $row[4],
            'section'     => $row[5],
            'sms_contact'     => $row[6],
            'father_contact'     => $row[7],
            'gender'     => $row[9],
            'DOB'     => $row[10],
            'mother_name'     => $row[11],
            'father_name'     => $row[12],
            'present_address'     => $row[13],
            'permanent_address'     => $row[14],
            'siblings_id'     => $row[15],
            'siblings'     => $row[16],
            'home_contact'     => $row[19],
            'guardian_contact'     => $row[20],
            'blood_group'     => $row[21],
            'email'     => $row[18],
            'nid'     => $row[0],

//            'class'     => $row['CLASS'],
//            'from_no'     => $row['FORM NO'],
//            'student_id'     => $row['ID'],
//            'student_name'     => $row['Student Name'],
//            'section'     => $row['Section'],
//            'sms_contact'     => $row['SMS Contact'],
//            'father_contact'     => $row['Fathers Contact'],
//            'gender'     => $row['Gender'],
//            'DOB'     => $row['DOB'],
//            'mother_name'     => $row['Mothers Name'],
//            'father_name'     => $row['Fathers Name'],
//            'present_address'     => $row['Present Address'],
//            'permanent_address'     => $row['Permanent Address'],
//            'siblings_id'     => $row['Siblings ID'],
//            'siblings'     => $row['Siblings Name'],
//            'home_contact'     => $row['Home Contact'],
//            'guardian_contact'     => $row['Home Contact'],
//            'blood_group'     => $row['Blood Group'],
//            'email'     => $row['EMAIL'],
//            'nid'     => $row['Sl.'],
//
//
        ]);
    }
}
