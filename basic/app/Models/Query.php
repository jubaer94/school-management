<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Query extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable= ['campus','user_id','item','query_no'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
