<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable =['class','from_no','student_id','student_name','section','sms_contact','father_contact','gender','DOB','mother_name','father_name','present_address','permanent_address','siblings_id','siblings','email','home_contact','guardian_contact','blood_group','nid'];
}
